from music21 import roman, chord
import pymongo


# takes music21.Score and slice - tuple (start,end)
# generator, yields tonal function of next chord in score - string
def tonal_functions_from_score(my_score, slice=None):
    if slice: # take bars from start to end of slice
        my_score = my_score.measures(slice[0], slice[1])

    k = my_score.analyze('key')  # extract key of score
    chfy = my_score.chordify()  # regroup notes in chords
    for c in chfy.flat.getElementsByClass(chord.Chord):  # each chord
        yield roman.romanNumeralFromChord(c, k).figure  # will be functionalized


# db = pymongo.MongoClient()
# chord_func_collection = db['music21-corpus']['chordFunctions']