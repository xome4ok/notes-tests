from collections import defaultdict
from itertools import zip_longest


def find_ngrams(input_list, n):
    true_ngrams = zip_longest(*[
        input_list[i:] for i in range(n)
        ])
    true_ngrams = list(true_ngrams)
    return [tuple([None] * (n-i) + list(true_ngrams[0][:i])) for i in range(1, n)] + true_ngrams



a = [1,2,3,4,5,6,7,8,9,10]

print(list(find_ngrams(a,4)))

class WNGram(object):
    def __init__(self, lst, n=3):
        self.length = None
        self.n = n
        self.table = defaultdict(int)
        self.parse_list(lst)
        self.calculate_length()

    def parse_list(self, lst):
        for ng in find_ngrams(lst, self.n):
            self.table[ng] += 1

    def calculate_length(self):
        """ Treat the N-Gram table as a vector and return its scalar magnitude
        to be used for performing a vector-based search.
        """
        self.length = sum([x * x for x in self.table.values()]) ** 0.5
        return self.length

    def __sub__(self, other):
        """ Find the difference between two NGram objects by finding the cosine
        of the angle between the two vector representations of the table of
        N-Grams. Return a float value between 0 and 1 where 0 indicates that
        the two NGrams are exactly the same.
        """
        if not isinstance(other, WNGram):
            raise TypeError("Can't compare NGram with non-NGram object.")

        if self.n != other.n:
            raise TypeError("Can't compare NGram objects of different size.")

        total = 0
        for k in self.table:
            total += self.table[k] * other.table[k]

        return 1.0 - (float(total) / (float(self.length) * float(other.length)))

    def find_match(self, groups):
        """ Out of a list of NGrams that represent individual groups, return
        the best match.
        """
        return min(groups, key=lambda n: self - n)

