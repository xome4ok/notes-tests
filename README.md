# README #

Trying to make automatically generated chord progressions.

Idea is the following: 

1. analyze existing corpus of music in musicXML format
2. get information about chord progerssion in this corpus
3. store it somewhere (mongodb)
4. using n-gram method predict next chord, based on n previous ones.

### Requirements ###

* python3.4
* mongodb (with database 'music21-corpus' created)
* music21 package from web.mit.edu/music21/