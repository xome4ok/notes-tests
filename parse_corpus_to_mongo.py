# requires mongodb up and running with db_name created
#
#  script for mongo:
#   use %db_name%
# it will automatically create %db_name% DB.
# all collections in DB are created on insert, so there's no need in creating them manually
#
# takes media21.metadata.bundles.MetadataBundle, string, string
# inserts everything from metadata_bundle to collection_name in db_name
from corpus_interaction import tonal_functions_from_score
from music21 import corpus


def copy_from_corpus_to_mongo(metadata_bundle, db_name, collection_name):
    from pymongo import MongoClient
    client = MongoClient()
    collection = client[db_name][collection_name]

    i = 1
    for next_score in metadata_bundle:
        parsed = next_score.parse()
        l = list(tonal_functions_from_score(parsed))
        collection.insert({'corpusPath': next_score.corpusPath,
                           'key': parsed.analyze('key').tonicPitchNameWithCase,
                           'chordFuncs': l
                           })
        print("Inserted %s items" % i)
        i += 1

if __name__ == '__main__':
    copy_from_corpus_to_mongo(corpus.search(''), 'python', 'chordFunctions')